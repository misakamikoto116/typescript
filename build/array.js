"use strict";
// Array => number
var array = [1, 2, 3];
array = [4, 5, 6];
// Array => String
var array2;
array2 = ['Joel', 'Effendi'];
// Array => Any
var array3;
array3 = [1, 'Julian', true, {}];
// Array Tuples (Define the data inside array)
var biodata;
biodata = ['Julian', 19];
