"use strict";
// Bagian integer seharusnya gk ada pake .length, typescript tidak bisa membaca karena disini dia type any
// Tidak ada error ketika menggunakan .length di integer (Seharusnya ada)
function getData(value) {
    return value;
}
console.log(getData("Julian").length);
console.log(getData(123).length);
// Generic
// Ada error ketika pake .length di integer (Type data nya tidak any tapi dinamis)
// T => bisa apapun terserah
function myData(value) {
    return value;
}
console.log(myData("Julian").length);
console.log(myData(123));
// <T, > => <T, unknow> (klo pake jsx ya gini caranya)
var arrowFunc = function (value) {
};
