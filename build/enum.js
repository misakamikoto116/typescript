"use strict";
// enum
// Berarti Key Feb = 2 dan selanjutnya
// numeric enum
var Month;
(function (Month) {
    Month[Month["JAN"] = 1] = "JAN";
    Month[Month["FEB"] = 2] = "FEB";
    Month[Month["MAR"] = 3] = "MAR";
    Month[Month["APR"] = 4] = "APR";
    Month[Month["MAY"] = 5] = "MAY";
})(Month || (Month = {}));
console.log(Month);
// String Enum
// Gk bisa salah satu aja kaya numeric
var StringMonth;
(function (StringMonth) {
    StringMonth["JAN"] = "Januari";
    StringMonth["FEB"] = "Februari";
    StringMonth["MAR"] = "Maret";
    StringMonth["APR"] = "April";
    StringMonth["MAY"] = "Mei";
})(StringMonth || (StringMonth = {}));
console.log(StringMonth);
