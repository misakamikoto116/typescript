"use strict";
var Asus = /** @class */ (function () {
    function Asus(name, isGaming) {
        this.name = name;
        this.isGaming = isGaming;
    }
    Asus.prototype.on = function () {
        console.log('Nyala', this.name);
    };
    Asus.prototype.off = function () {
        console.log('Mati', this.name);
    };
    return Asus;
}());
var Macbook = /** @class */ (function () {
    function Macbook(name, keyboardLight) {
        this.name = name;
        this.keyboardLight = keyboardLight;
    }
    Macbook.prototype.on = function () {
        console.log('Nyala', this.name);
    };
    Macbook.prototype.off = function () {
        console.log('Mati', this.name);
    };
    return Macbook;
}());
var asus = new Asus("ROG", true);
console.log(asus.on());
console.log(asus.off());
var mac = new Macbook("Pro 2015", true);
console.log(mac.on());
console.log(mac.off());
