"use strict";
// String
var nama = 'Julian';
nama = 'Joel';
// Number
var umur = 19;
umur = 20;
// Boolean
var isMarried = true;
isMarried = false;
// Any
var heroes = 'Iron Man';
heroes = false;
// union type
// 0 di belakang dibaca tidak ada jika number, maka gunakan string
var phone;
phone = 62823505023;
phone = "082350530023";
