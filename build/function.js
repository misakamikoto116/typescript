"use strict";
// tipe data pada balikan function
// String
function getName() {
    return "Its Joel " + 123;
}
console.log(getName());
// Number
function getAge() {
    return 19;
}
console.log(getAge());
// Void => Gk boleh return, Sebenarnya klo gk ada return udh automatis void
function printName() {
    console.log('void Name Print');
}
printName();
/////////////////
// val 1 = number, val 2 = number, multiply = number
function multiply(val1, val2) {
    return val1 * val2;
}
var result = multiply(2, 10);
console.log(result);
var add = function (val1, val2) {
    return val1 * val2;
};
console.log(add(33, 2));
// default parameter
var fullName = function (first, last) {
    if (last === void 0) { last = 'Effendi'; }
    return first + " " + last;
};
console.log(fullName("Julian"));
// Optional Parameter
// Jika val2 gk diisi jadi undefined
// Klo ada optional (val2?) klo bisa return nya jangan number, karena klo penjumlahan ada error kemungkkinan undefined (Number + undenfined = NaN)
var getUmur = function (val1, val2) {
    return val1 + " " + val2;
};
console.log(getUmur(1, 120));
