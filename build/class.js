"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var User = /** @class */ (function () {
    function User(name, age) {
        this.age = age;
        this.name = name;
    }
    User.prototype.setName = function (value) {
        this.name = value;
    };
    User.prototype.getName = function () {
        return this.name;
    };
    return User;
}());
exports.User = User;
var user = new User("Julian", 19);
console.log(user);
// public = Bisa diakses di semua class / dari luar class
// protected = hanya bisa diakses dari class tersebut, dan class turunan
// private = hanya bisa diakses dari class itu sendiri
// Dikasih private _email agar tidak bisa langsung ambil, hanya bisa di ambil lewat getter / get
var Admin = /** @class */ (function (_super) {
    __extends(Admin, _super);
    function Admin(phone, name, age) {
        var _this = _super.call(this, name, age) || this;
        _this.write = true;
        _this.read = true;
        _this._email = "";
        _this.phone = phone;
        return _this;
    }
    Admin.getNameRole = function () {
        return "Hai";
    };
    Admin.prototype.getRole = function () {
        return {
            write: this.write,
            read: this.read
        };
    };
    Object.defineProperty(Admin.prototype, "email", {
        get: function () {
            return this._email;
        },
        set: function (value) {
            if (value.length < 5) {
                this._email = 'Email Harus lebih dari 5 huruf';
            }
            else {
                this._email = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Admin.getRoleName = 'Admin';
    return Admin;
}(User));
// Constuct User nya karena extends
var admin = new Admin("082350530023", "Joel", 19);
var role = admin.getRole();
admin.setName("Julian");
var name = admin.getName();
console.log(role, name);
admin.phone;
admin.email = 'Julian@gmail.com';
console.log(admin.email);
// static
var roleName = Admin.getRoleName;
var RolenameFnct = Admin.getNameRole();
console.log(roleName, RolenameFnct);
