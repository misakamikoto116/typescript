// tipe data pada balikan function

// String
function getName(): string {
    return "Its Joel " + 123
}

console.log(getName())

// Number
function getAge(): number {
    return 19;
}

console.log(getAge())


// Void => Gk boleh return, Sebenarnya klo gk ada return udh automatis void
function printName(): void {
    console.log('void Name Print')
}

printName()


/////////////////

// val 1 = number, val 2 = number, multiply = number
function multiply(val1: number, val2: number): number {
    return val1 * val2;
}

const result = multiply(2, 10);
console.log(result);

// function as type
// Jika menggunakan type ini maka rules harus mengikuti sama seperti tambah, => number maksudnya return harus number
type Tambah = (val1: number, val2: number) => number;

const add: Tambah = (val1: number, val2: number): number => {
    return val1 * val2;
}

console.log(add(33, 2));



// default parameter
const fullName = (first: string, last: string = 'Effendi'): string => {
    return first + " " + last;
}

console.log(fullName("Julian"))


// Optional Parameter
// Jika val2 gk diisi jadi undefined
// Klo ada optional (val2?) klo bisa return nya jangan number, karena klo penjumlahan ada error kemungkkinan undefined (Number + undenfined = NaN)
const getUmur = (val1: number, val2?: number): string => {
    return val1 + " " + val2;
}

console.log(getUmur(1, 120))