// object
type User = {
    name: string,
    age: number
}

let user: User = {
    name: "Julian",
    age: 19
}