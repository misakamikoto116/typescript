export class User {
    public name: string;

    constructor(name: string, public age: number) {
        this.name = name;
    } 

    setName(value: string): void {
        this.name = value
    }

    getName(): string {
        return this.name
    }
}

let user = new User("Julian", 19);
console.log(user)

// public = Bisa diakses di semua class / dari luar class
// protected = hanya bisa diakses dari class tersebut, dan class turunan
// private = hanya bisa diakses dari class itu sendiri


// Dikasih private _email agar tidak bisa langsung ambil, hanya bisa di ambil lewat getter / get
class Admin extends User {
    write: boolean = true;
    read: boolean = true;
    phone: string;
    private _email:string = "";
    static getRoleName: string = 'Admin';

    constructor(phone: string, name: string, age: number) {
        super(name, age);
        this.phone = phone;
    }

    static getNameRole(): string {
        return "Hai"
    }

    getRole(): { write: boolean, read: boolean } {
        return {
            write: this.write,
            read: this.read
        }
    }

    set email(value: string) {
        if (value.length < 5) {
            this._email = 'Email Harus lebih dari 5 huruf';
        } else {
            this._email = value;
        }
    }

    get email(): string {
        return this._email;
    }
}

// Constuct User nya karena extends
let admin = new Admin("082350530023", "Joel", 19)
let role = admin.getRole();
admin.setName("Julian");
let name = admin.getName();
console.log(role, name)

admin.phone;
admin.email = 'Julian@gmail.com';
console.log(admin.email)


// static
let roleName = Admin.getRoleName;
let RolenameFnct = Admin.getNameRole();
console.log(roleName, RolenameFnct);