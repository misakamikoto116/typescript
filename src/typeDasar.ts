// String
let nama: string = 'Julian';

nama = 'Joel';

// Number
let umur: number = 19;
umur = 20

// Boolean
let isMarried: boolean = true;
isMarried = false;


// Any
let heroes: any = 'Iron Man';
heroes = false;

// union type
// 0 di belakang dibaca tidak ada jika number, maka gunakan string
let phone: number | string;
phone = 62823505023;
phone = "082350530023";