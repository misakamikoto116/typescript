// enum
// Berarti Key Feb = 2 dan selanjutnya
// numeric enum
enum Month {
    JAN = 1,
    FEB,
    MAR,
    APR,
    MAY
}

console.log(Month)

// String Enum
// Gk bisa salah satu aja kaya numeric
enum StringMonth {
    JAN = 'Januari',
    FEB = 'Februari',
    MAR = 'Maret',
    APR = 'April',
    MAY = 'Mei'
}

console.log(StringMonth)