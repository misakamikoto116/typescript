// Array => number
let array: number[] = [1, 2, 3];
array = [4, 5, 6];

// Array => String
let array2: string[];
array2 = ['Joel', 'Effendi'];

// Array => Any
let array3: any[];
array3 = [1, 'Julian', true, {}];


// Array Tuples (Define the data inside array)
let biodata: [string, number];
biodata = ['Julian', 19];